package com.java.response;

public class Response {

	private String country;	
	private String city;
	private float avgIncome;
	private String gender;

	public Response(String country, String city, float avgIncome, String gender) {
		this.country = country;
		this.city = city;
		this.avgIncome = avgIncome;
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}

	public String getCity() {
		return city;
	}

	public float getAvgIncome() {
		return avgIncome;
	}

	public String getGender() {
		return gender;
	}

	@Override
	public String toString() {
		return "Response [country=" + country + ", city=" + city + ", avgIncome=" + avgIncome + ", gender=" + gender
				+ "]";
	}
	
	
}
