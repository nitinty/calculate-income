package com.java.factory;

public class ReadCalulateAvgFactory {

	public static Reader getReader(String readerType) {
		
		Reader reader = null;
		if (readerType.equalsIgnoreCase("CSV")) {
			reader = new CSVReader();
		} else if (readerType.equalsIgnoreCase("Excel")) {
			reader = new ExcelReader();
		}  
		return reader;
	}
}
