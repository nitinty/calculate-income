package com.java.factory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CSVReader implements Reader {

	@Override
	public Map<String,Float> readCalulateAvg() {
		
		String csvFile = "../calculateIncome/src/resources/sampleInput.csv";
	    String line = "";
	    String cvsSplitBy = ",";
	    
	    Map<String,Float> countryMap=new HashMap<>();
	
	    try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
	    	br.readLine();
	        while ((line = br.readLine()) != null) {
	            // use comma as separator
	            String[] data = line.split(cvsSplitBy);
	            
				String city=data[0];
				String country=data[1];
				String gender=data[2];
				String currency=data[3];
				int averageIncome=Integer.parseInt(data[4]);
				float covertAvgIncome = currencyConverter(currency, averageIncome);
				
				if(country.equals("")){
					
					if(countryMap.containsKey(city+","+gender+",CI")){
						float avg=countryMap.get(city+","+gender+",CI");
						avg=avg+covertAvgIncome;
						countryMap.put(city+","+gender+",CI",avg);
					
					}else{
						countryMap.put(city+","+gender+",CI",covertAvgIncome);
					}
				} else{
					if(countryMap.containsKey(country+","+gender+",CO")){
		    		
						float avg=countryMap.get(country+","+gender+",CO");
						avg=avg+covertAvgIncome;
						countryMap.put(country+","+gender+",CO",avg);
					}else{
						countryMap.put(country+","+gender+",CO",covertAvgIncome);
					}
				}
	        }
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
		
		return countryMap;
	}

	private float currencyConverter(String currency, int averageIncome) {
		float convertAvgIncome = (float) 0.0;
		
		if(currency.equals("INR")){
			convertAvgIncome=averageIncome/66;
		} else if(currency.equals("GBP")) {
			convertAvgIncome=(float) (averageIncome/0.67);
		}else if(currency.equals("SGP")){
			convertAvgIncome=(float) (averageIncome/1.5);
		}else if(currency.equals("HKD")){
			convertAvgIncome=averageIncome/8;
		}else if(currency.equals("USD")){
			convertAvgIncome=averageIncome;
		}
		return convertAvgIncome;
	}
}