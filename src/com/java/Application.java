package com.java;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.java.factory.ReadCalulateAvgFactory;
import com.java.factory.Reader;
import com.java.response.Response;

public class Application {

	public static void main(String[] args) {
		Reader reader = ReadCalulateAvgFactory.getReader("CSV");
		
		Map<String, Float> readCalulateAvg = reader.readCalulateAvg();
		
		List<Response> responses = new ArrayList<>();
		
		for(Map.Entry<String,Float> ms:readCalulateAvg.entrySet()){
			
			String key=ms.getKey();
			float value=ms.getValue();
			String responseArray[]=key.split(",");
			
			if("CI".equals(responseArray[2])) {
				responses.add(new Response("", responseArray[0], value, responseArray[1]));
			}else if("CO".equals(responseArray[2])) {
				responses.add(new Response(responseArray[0], "", value, responseArray[1]));
			}
			
		   }
		
		Collections.sort(responses, Comparator.comparing(Response::getCountry)
				.thenComparing(Response::getCity).thenComparing(Response::getAvgIncome));
		
		
		writeCsv(responses);
		
		System.out.println(responses);
	}
	
	private static void writeCsv(List<Response> responses) {
		
		try (PrintWriter writer = new PrintWriter(new File("../calculateIncome/src/resources/sampleOutput.csv"))) {

		     StringBuilder sb = new StringBuilder();
		     sb.append("Country,");
		     sb.append("City,");
		     sb.append("Gender,");
		     sb.append("AvgIncome");
		     sb.append('\n');
		     
		     responses.stream().peek(obj->{
		    	 sb.append(obj.getCountry()+",");
		    	 sb.append(obj.getCity()+",");
		    	 sb.append(obj.getGender()+",");
		    	 sb.append(obj.getAvgIncome()+",");
		    	 sb.append('\n');
		     }).count();
		     
		     writer.write(sb.toString());

		     System.out.println("done!");

		   } catch (FileNotFoundException e) {
		     System.out.println(e.getMessage());
		   }
		}
}
